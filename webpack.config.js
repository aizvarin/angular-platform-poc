module.exports = {
    context: __dirname + '/app',
    entry: [
        './index.js'
    ],
    output: {
        path: __dirname,
        filename: './bundle.js'
    },
    module: {
    loaders: [
        { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },
        { test: /\.html$/, loader: 'html' },

        { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },

        { test: /\.(woff|woff2)$/, loader: "url?limit=10000&minetype=application/font-woff" },
        { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream" },
        { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
        { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=image/svg+xml" }
    ]
  }
}
