import 'bootstrap-webpack';
import 'style!css!../assets/css/dashboard.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';
import routing from './app.config';
import movies from './modules/movies';
import territories from './modules/territories';
import actors from './modules/actors';

angular.module('platformApp', [uirouter, movies, territories, actors])
    .config(routing);
