export default class MoviesController {
    constructor($scope) {
        $scope.movies = [
            { title: 'The Travelling Whore', 'code': '1033-10234-000' },
            { title: 'Batman', 'code': '1028-11987-000' },
            { title: 'The Matrix', 'code': '1041-19232-000' },
            { title: 'Simpsons Series', 'code': '1020-13336-000' },
            { title: 'Futurama Series', 'code': '1098-12773-000' }
        ];
    }

    createMovie () {

    }

    deleteMovie (movieCode) {

    }
}
