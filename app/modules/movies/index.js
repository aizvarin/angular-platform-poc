import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './movies.routes';
import MoviesController from './movies.controller';
import MovieController from './movie.controller';

export default angular.module('platformApp.movies', [uirouter])
    .config(routing)
    .controller('MoviesController', MoviesController)
    .controller('MovieController', MovieController)
    .name;
