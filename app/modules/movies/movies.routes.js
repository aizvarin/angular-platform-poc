routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    var movies = {
        name: 'movies',
        abstract: true,
        url: '/movies',
        template: require('./movies.view.html'),
        controller: 'MoviesController'
    };

    var moviesList = {
        name: 'movies.list',
        parent: movies,
        url: '/',
        template: require('./movies.list.view.html')
    };

    var moviesEdit = {
        name: 'movies.edit',
        parent: movies,
        url: '/:movieCode',
        template: require('./movie.view.html'),
        controller: 'MovieController'
    };

    $stateProvider
        .state(movies)
        .state(moviesList)
        .state(moviesEdit);
}
