import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './territories.routes';
import TerritoriesController from './territories.controller';

export default angular.module('platformApp.territories', [uirouter])
    .config(routing)
    .controller('TerritoriesController', TerritoriesController)
    .name;
