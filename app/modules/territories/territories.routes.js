routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    var territories = {
        name: 'territories',
        url: '/territories',
        template: require('./territories.view.html'),
        controller: 'TerritoriesController'
    };

    $stateProvider.state(territories);
}
