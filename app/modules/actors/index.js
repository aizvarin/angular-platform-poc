import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './actors.routes';
import ActorsController from './actors.controller';

export default angular.module('platformApp.actors', [uirouter])
    .config(routing)
    .controller('ActorsController', ActorsController)
    .name;
