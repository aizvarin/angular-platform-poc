routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    var actors = {
        name: 'actors',
        url: '/actors',
        template: require('./actors.view.html'),
        controller: 'ActorsController'
    };

    $stateProvider.state(actors);
}
